# Ggplot2 - an interactive Jupyter-Notebook tutorial for R

An interactive Jupyter-Notebook Tutorial for the Ggplot2 package in R

Mybinder link:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/RichardWin%2Fggplot2-an-interactive-jupyter-notebook-tutorial-for-r/main)
